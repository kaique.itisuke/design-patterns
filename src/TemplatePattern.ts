import { consoleGreen, consoleYellow } from "./utils";
import { validate, IsDate, IsString, Contains, Length } from "class-validator";

interface Person {
  id?: number;
  name: string;
  emoji: string;
}

class Student implements Person {
  id?: number;

  @IsString()
  @Length(3, 60)
  name: string;

  @IsString()
  @Length(1)
  emoji: string;

  @IsDate()
  enrollmentDate: Date;

  constructor(name: string, emoji: string, enrollmentDate: Date) {
    this.name = name;
    this.emoji = emoji;
    this.enrollmentDate = enrollmentDate;
  }
}

class Instructor implements Person {
  @IsString()
  @Length(3, 60)
  name: string;

  @IsString()
  @Length(1)
  emoji: string;

  @IsString()
  @Length(3, 60)
  @Contains("Professor")
  academicRank: string;

  constructor(name: string, emoji: string, academicRank: string) {
    this.name = name;
    this.emoji = emoji;
    this.academicRank = academicRank;
  }
}

abstract class PersonService {
  protected abstract intanciateNew(): Person;

  protected validate(person: Person) {
    console.log("Validating✅...");

    validate(person).then((errors) => {
      if (errors.length > 0) {
        throw Error(`validation failed❌. errors: ${errors}`);
      }
    });
  }

  protected persist(person: Person) {
    console.log("Saving💾...");
    person.id = Math.floor(Math.random() * 1000) + 1;
  }

  protected showMessage(person: Person) {
    consoleGreen(
      `The person: ID {${person.id}} ${person.name}${person.emoji} has been saved🔚...\n`
    );
  }

  public create() {
    const person = this.intanciateNew();
    this.validate(person);
    this.persist(person);
    this.showMessage(person);
  }
}

class StudentService extends PersonService {
  protected intanciateNew(): Person {
    return new Student("Joana", "🙋", new Date());
  }
}

class InstructorService extends PersonService {
  protected intanciateNew(): Person {
    return new Instructor("José da Silva", "👮", "Professor");
  }
}

export function bootTemplate(): void {
  consoleYellow("\nTemplate 👪 Pattern:");
  console.group();
  consoleGreen(
    "Is a method in a superclass, usually an abstract superclass,\nand defines the skeleton of an operation in terms of a number of high-level steps.\nThese steps are themselves implemented by additional helper methods\nin the same class as the template method.\n"
  );

  const studentService = new StudentService();
  studentService.create();

  const instructorService = new InstructorService();
  instructorService.create();

  console.groupEnd();
}
