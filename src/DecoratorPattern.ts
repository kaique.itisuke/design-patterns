import { consoleGreen, consoleYellow } from "./utils";

interface Food {
  getCost(): number;
  getDescription(): string;
}

class BasicMeal implements Food {
  getCost(): number {
    return 8;
  }

  getDescription(): string {
    return "Basic Meal🍴";
  }
}

class Salad implements Food {
  protected food: Food;

  constructor(food: Food) {
    this.food = food;
  }

  getCost(): number {
    return this.food.getCost() + 2;
  }

  getDescription(): string {
    return `${this.food.getDescription()} + Salad🍅`;
  }
}

class Steak implements Food {
  protected food: Food;

  constructor(food: Food) {
    this.food = food;
  }

  getCost(): number {
    return this.food.getCost() + 8;
  }

  getDescription(): string {
    return `${this.food.getDescription()} + Steak🍖`;
  }
}

export function bootDecorator(): void {
  consoleYellow("Decorator 🎄 Pattern:");
  console.group();
  consoleGreen(
    "Allows behavior to be added to an individual object, dynamically,\nwithout affecting the behavior of other objects from the same class.\n"
  );

  const basicMeal = new BasicMeal();
  console.info(`The cost of a ${basicMeal.getDescription()}:😄`);
  consoleGreen(`$${basicMeal.getCost()}👍`);

  const salad = new Salad(basicMeal);
  console.info(`The cost of a ${salad.getDescription()}:😃`);
  consoleGreen(`$${salad.getCost()}👌`);

  const steak = new Steak(salad);
  console.info(`The cost of a ${steak.getDescription()}:😋`);
  consoleGreen(`$${steak.getCost()}😱`);

  console.groupEnd();
}
