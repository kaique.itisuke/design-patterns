import { consoleGreen, consoleRed, consoleYellow } from "./utils";

class HomeStatus {
  locked: boolean;
  lightsOff: boolean;
  alarmOn: boolean;

  constructor(locked: boolean, lightsOff: boolean, alarmOn: boolean) {
    this.locked = locked;
    this.lightsOff = lightsOff;
    this.alarmOn = alarmOn;
  }
}

abstract class HomeChecker {
  public abstract check(home: HomeStatus): void;
  protected sucessor?: HomeChecker;

  setSucessor(sucessor: HomeChecker) {
    this.sucessor = sucessor;
  }

  next(home: HomeStatus) {
    this.sucessor?.check(home);
  }
}

class Locks extends HomeChecker {
  public check(home: HomeStatus): void {
    console.info("↪️  Checking the locks🔒...");

    if (!home.locked) {
      consoleRed("The doors🚪 are not locked!!!❌");
    }

    this.next(home);
  }
}

class Lights extends HomeChecker {
  public check(home: HomeStatus): void {
    console.info("➡️  Checking the lights💡...");

    if (!home.lightsOff) {
      consoleRed("The lights💡 are still on!!!❌");
    }

    this.next(home);
  }
}

class Alarm extends HomeChecker {
  public check(home: HomeStatus): void {
    console.info("➡️  Checking the alarm📠...");

    if (!home.alarmOn) {
      consoleRed("The alarm📠 has not been set!!!❌");
    }
  }
}

export function bootChainOfResponsability(): void {
  consoleYellow("\nChain of Responsability ↪️ ➡️ ➡️ ➡️  Pattern:");
  console.group();
  consoleGreen(
    'Encapsulate the processing elements inside a "pipeline" abstraction. Each processing object contains logic that defines the types of command objects that it can handle; the rest are passed to the next processing object in the chain.\n'
  );

  const home = new HomeStatus(true, true, false);

  const locks = new Locks();
  const lights = new Lights();
  const alarm = new Alarm();

  locks.setSucessor(lights);
  lights.setSucessor(alarm);

  locks.check(home);

  console.groupEnd();
}
