import { consoleGreen, consoleYellow } from "./utils";

enum errorTypes {
  INFO,
  WARNING,
  ERROR,
}

interface DataToLog {
  message: string;
  type: errorTypes;
}

interface Logger {
  log(data: DataToLog): void;
}

class LogToFile implements Logger {
  log(data: DataToLog) {
    console.log(`Loggind msg: "${data.message}" to a file📋`);
  }
}

class LogToRedis implements Logger {
  log(data: DataToLog) {
    console.log(`Loggind msg: "${data.message}" to a redis🏮 database`);
  }
}

class LogToPostgress implements Logger {
  log(data: DataToLog) {
    console.log(`Loggind msg: "${data.message}" to a postgress🐘 database`);
  }
}

class App {
  log(data: DataToLog, logger?: Logger) {
    logger = logger ?? new LogToFile();
    logger.log(data);
  }
}

export function bootStrategy(): void {
  consoleYellow("Strategy 🚕🚗🚓 Pattern:");
  console.group();
  consoleGreen(
    "Define a family of algorithms, encapsulate and make them interchangeable.\n"
  );

  const app = new App();

  consoleGreen("Log to File:");
  app.log({
    message: "Server has been started at localhost:4000",
    type: errorTypes.INFO,
  });

  consoleGreen("Log to Redis:");
  app.log(
    {
      message: "User with ID 1 not found",
      type: errorTypes.WARNING,
    },
    new LogToRedis()
  );

  consoleGreen("Log to Postgress:");
  app.log(
    {
      message: "Error uploading the X file",
      type: errorTypes.ERROR,
    },
    new LogToPostgress()
  );

  console.groupEnd();
}
