import { bootDecorator } from "./DecoratorPattern";
import { bootAdapter } from "./AdapterPattern";
import { bootTemplate } from "./TemplatePattern";
import { bootStrategy } from "./StrategyPattern";
import { bootChainOfResponsability } from "./ChainOfResponsabilityPattern";

bootDecorator();
bootAdapter();
bootTemplate();
bootStrategy();
bootChainOfResponsability();
