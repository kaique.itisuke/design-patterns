import { consoleGreen, consoleYellow } from "./utils";

interface Book {
  open(): void;
  turnPage(): void;
}

interface EReader {
  turnOn(): void;
  pressNextButton(): void;
}

class PaperBook implements Book {
  open() {
    console.info("Opening the paper book📖");
  }

  turnPage() {
    console.info("Turning the page of the paper book📖👋");
  }
}

class Kindle implements EReader {
  turnOn() {
    console.info("Turning the Kindle📟 on");
  }

  pressNextButton() {
    console.info("Pressing the next button on the Kindle👇📟");
  }
}

class Kobo implements EReader {
  turnOn() {
    console.info("Turning the Kobo📱 on");
  }

  pressNextButton() {
    console.info("Pressing the next button on the Kobo👇📱");
  }
}

class Person {
  read(book: Book) {
    book.open();
    book.turnPage();
  }
}

class EReaderAdatper implements Book {
  private eReader: EReader;

  constructor(eReader: EReader) {
    this.eReader = eReader;
  }

  open(): void {
    this.eReader.turnOn();
  }

  turnPage(): void {
    this.eReader.pressNextButton();
  }
}

export function bootAdapter(): void {
  consoleYellow("\nAdapter 🔌 Pattern:");
  console.group();
  consoleGreen(
    "An adapter allows you to translate one interface for use with another."
  );

  const person = new Person();
  consoleGreen("\nI want to read a paper book📖!");
  const book = new PaperBook();
  person.read(book);

  consoleGreen("\nI want to read a Kindle📟 book!");
  const kindle = new Kindle();
  const kindleAdapter = new EReaderAdatper(kindle);
  person.read(kindleAdapter);

  consoleGreen("\nI want to read a Kobo📱 book!");
  const kobo = new Kobo();
  const koboAdapter = new EReaderAdatper(kobo);
  person.read(koboAdapter);

  console.groupEnd();
}
