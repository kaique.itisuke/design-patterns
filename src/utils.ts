// ANSI Terminal Colors Codes: https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
const consoleCodeGreen = 32;
const consoleCodeYellow = 33;
const consoleCodeRed = 31;

function consoleWithColor(string: string, colorCode: number) {
  console.info(`\x1b[${colorCode}m%s\x1b[0m`, string);
}

export function consoleGreen(string: string) {
  consoleWithColor(string, consoleCodeGreen);
}

export function consoleYellow(string: string) {
  consoleWithColor(string, consoleCodeYellow);
}

export function consoleRed(string: string) {
  consoleWithColor(string, consoleCodeRed);
}
