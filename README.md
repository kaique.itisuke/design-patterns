# About The Project

This project was made with the purpose of explaining the main project patterns in the simplest way possible using Real world examples.

# Getting Started

This document describes how to set up your development environment and run the project locally.

- [Prerequisite Software](#prerequisite-software)
- [Running Locally](#running-locally)
- [Linting/verifying your Source Code](#linting)
- [Commit Message Format](#commit-message-format)

## Prerequisite Software

Before you can run this project, you must install and configure the
following software on your development machine:

- [Git](https://git-scm.com/) version control system;

- [Node.js 14+](https://nodejs.org), which is used to run a development web server;

- [NPM 7+](https://www.npmjs.com/package/npm) which is used to manage and install dependencies.

## Running Locally

```
$ npm i
$ npm start
```

## Linting/verifying your Source Code

```
$ npm run lint
```

## <a name="commit"></a> Commit Message Format

_This specification is based on [Conventional commits](https://www.conventionalcommits.org/en/v1.0.0/)._

We have very precise rules over how our Git commit messages must be formatted.
This format leads to **easier to read commit history**.

Each commit message consists of a **header**, a **body**, and a **footer**.

```
<header>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

Example:

```
fix(user): fixed a bug that causes the response to be undefined

The bug was happening when accessing the endpoint user/:id and the :id was not found.

The expected response when the id is not found in the database is an empty JSON object "{}".
```

The `header` is mandatory and must conform to the [Commit Message Header](#commit-header) format.

The `body` is mandatory for all commits except for those of type "docs".
When the body is present it must be at least 20 characters long and must conform to the [Commit Message Body](#commit-body) format.

The `footer` is optional. The [Commit Message Footer](#commit-footer) format describes what the footer is used for and the structure it must have.

Any line of the commit message cannot be longer than 100 characters.

#### <a name="commit-header"></a>Commit Message Header

```
<type>(<scope>): <short summary>
  │       │             │
  │       │             └─⫸ Summary in present tense. Not capitalized. No period at the end.
  │       │
  │       └─⫸ Commit Scope: pattern|tests
  │
  └─⫸ Commit Type: build|ci|chore|docs|feat|fix|perf|refactor|revert|style|test
```

The `<type>` and `<summary>` fields are mandatory, the `(<scope>)` field is optional.

Examples:

```
build: added husky dependency

feat: created find user by id endpoint

fix(user): fixed a bug that causes the response to be undefined
```

##### Type

Must be one of the following:

- **build**: Changes that affect the build system or external dependencies (example scopes: npm, gulp, webpack)
- **ci**: Changes to our CI configuration files and scripts
- **chore**: tool changes, configuration changes, and changes to things that do not actually go into production at all (Other changes that don't modify src or test files) (like the .gitignore or .gitattributes)
- **docs**: Documentation only changes
- **feat**: A new feature
- **fix**: A bug fix
- **perf**: A code change that improves performance
- **refactor**: A code change that neither fixes a bug nor adds a feature
- **revert**: If the commit reverts a previous commit
- **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
- **test**: Adding missing tests or correcting existing tests

##### Scope

The scope should be the name of the npm package affected (as perceived by the person reading the changelog generated from commit messages).

The following is the list of supported scopes:

- pattern
- tests

##### Summary

Use the summary field to provide a succinct description of the change:

- use the imperative, present tense: "change" not "changed" nor "changes"
- don't capitalize the first letter
- no dot (.) at the end

#### <a name="commit-body"></a>Commit Message Body

Just as in the summary, use the imperative, present tense: "fix" not "fixed" nor "fixes".

Explain the motivation for the change in the commit message body. This commit message should explain _why_ you are making the change.
You can include a comparison of the previous behavior with the new behavior in order to illustrate the impact of the change.

#### <a name="commit-footer"></a>Commit Message Footer

The footer can contain information about breaking changes and is also the place to reference GitHub issues, Jira tickets, and other PRs that this commit closes or is related to.

```
BREAKING CHANGE: <breaking change summary>
<BLANK LINE>
<breaking change description + migration instructions>
<BLANK LINE>
<BLANK LINE>
Fixes #<issue number>
```

Breaking Change section should start with the phrase "BREAKING CHANGE: " followed by a summary of the breaking change, a blank line, and a detailed description of the breaking change that also includes migration instructions.

### Revert commits

If the commit reverts a previous commit, it should begin with `revert: `, followed by the header of the reverted commit.

The content of the commit message body should contain:

- information about the SHA of the commit being reverted in the following format: `This reverts commit <SHA>`,
- a clear description of the reason for reverting the commit message.
